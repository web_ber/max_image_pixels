<?php
/**
 * Created by PhpStorm.
 * User: stager
 * Date: 04.07.2018
 * Time: 17:24
 */

$res = loadImage('image/img.jpg');

/**
 * @param $data
 * @return array
 */
function loadImage($data)
{
    if (!($img = imagecreatefromjpeg($data))) {
        die('Ошибка. Путь к картинке или ее наименование указано неверно');
    }
    $width = imagesx($img);
    $height = imagesy($img);
    $out = [];
    $i = 0; // высота
    $j = 0; // ширина
    while ($i < $height) {
        while ($j < $width) {
            $color = imagecolorsforindex($img, imagecolorat($img, $j, $i));
            $color = implode(',', $color);

            $out[] = $color;
            $j++;
        }
        unset($j);
        $i++;
    }

    // группировка цвета по количеству повторений
    $res = array_count_values($out);

    // переборка
    while ($curColor = current($res)) {
        if ($curColor == max($res)) {
            echo 'Наибольшее количество пикселей (' . $curColor . ') ' . 'с цветом rgba('.key($res) . ")\n";
        }
        next($res);
    }

    return $out;
}